# solarisin/particular

Particular is an air quality monitor application specifically for monitoring 3D printer spaces.

## Technologies Used
- [.NET 6.0](https://dotnet.microsoft.com/en-us/)
- [docker](https://www.docker.com/)
- [docker-compose](https://docs.docker.com/compose/)
- and more

## Installation

Utilizes docker-compose to build and deploy application code and required containers.

```bash
docker-compose up
```

## Contributing
This project is very much still in the starting stages and will evolve rapidly. Feel free to contribute wherever possible.

## License
[GNU GPLv3](https://choosealicense.com/licenses/gpl-3.0/)

